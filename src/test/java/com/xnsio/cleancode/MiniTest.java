package com.xnsio.cleancode;

import static junit.framework.TestCase.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.xnsio.cleancode.Formatter;
import com.xnsio.cleancode.Meeting;
import com.xnsio.cleancode.MeetingAssistant;
import com.xnsio.cleancode.MeetingScheduler;

public class MiniTest {
	private static final String BUSY = "Busy";
	private static final String DATE = "EEEE dd, h:mm a";
	private static final String PERSON1 = "P1";
	private static final String PERSON2 = "P2";
	private static final String LUNCH = "Lunch";
	private static final String TEAMPLANNING = "TeamPlanning";
	private static final String MEET_MINI = "Meet_Mini";
	private static final String MEET_FRANK = "Meet_Frank";
	private static final String MEET_BRAD = "Meet_Brad";
	private static final String MEET_JANET = "Meet_Janet";
	private static final String MINI = "Mini";
	private static final String BRAD = "Brad";
	private static final String JANET = "Janet";
	private static final String FRANK = "Frank";
	private static final String FULL_BUSY_PERSON = "FullBusyPerson";
	LocalTime[] slotsArray = new LocalTime[] { Meeting.EIGHT, Meeting.NINE, Meeting.TEN,
			Meeting.ELEVEN, Meeting.TWELVE, Meeting.THIRTEEN, Meeting.FOURTEEN,
			Meeting.FIFTEEN, Meeting.SIXTEEN };
	private Map<String, MeetingScheduler> attendeesSchedule;
	private List<String> meetingInvitee = Arrays.asList(PERSON2, PERSON1);

	@Before
	public void setUp() {
		attendeesSchedule = new HashMap<>();
		attendeesSchedule.put(PERSON2, new MeetingScheduler(LocalDate.now()));
		attendeesSchedule.put(PERSON1, new MeetingScheduler(LocalDate.now()));
	}

	@Test
	public void scheduleMeetingWithSelf() {
		MeetingAssistant assistant = new MeetingAssistant(attendeesSchedule, new Formatter(DATE));
		LocalDateTime availableTime = assistant.getCommonTimeSlot(Arrays.asList(PERSON2), LocalDate.now());
		assertEquals(LocalDateTime.of(LocalDate.now(), LocalTime.of(8, 0)), availableTime);
	}

	@Test
	public void getDefaultTimeSlot() {
		MeetingAssistant assistant = new MeetingAssistant(attendeesSchedule, new Formatter(DATE));
		LocalDateTime availableTime = assistant.getCommonTimeSlot(meetingInvitee, LocalDate.now());
		assertEquals(LocalDateTime.of(LocalDate.now(), LocalTime.of(8, 0)), availableTime);
	}

	@Test
	public void returnNextAvailableTimeSlotIfDefaultIsBusy() {
		attendeesSchedule.put(PERSON2,
				prepareBusySchedule(LocalDate.now(), new LocalTime[] { Meeting.EIGHT }));
		MeetingAssistant assistant = new MeetingAssistant(attendeesSchedule, new Formatter(DATE));
		LocalDateTime availableTime = assistant.getCommonTimeSlot(meetingInvitee, LocalDate.now());
		assertEquals(LocalDateTime.of(LocalDate.now(), Meeting.NINE), availableTime);
	}
	
	@Test
	public void janeMeetingWithBusyBee() {
		Map<String, MeetingScheduler> employeeSchedule = meetingScheduleForTeam();
		employeeSchedule.put(FULL_BUSY_PERSON, prepareBusyScheduleFor(LocalDate.now().plusDays(30)));
		MeetingAssistant assistant = new MeetingAssistant(employeeSchedule, new Formatter(DATE));
		String availableTime = assistant.searchCommonSlotDetails(Arrays.asList(FRANK, FULL_BUSY_PERSON),
				LocalDate.now().plusDays(30));
		assertEquals("No available time-slot for Frank and FullBusyPerson to meet.", availableTime);
	}
	
	private MeetingScheduler prepareBusyScheduleFor(LocalDate todaysDay) {
		MeetingScheduler meetingSchedule = new MeetingScheduler(todaysDay);
		for (LocalTime slot : Meeting.slots) {
			meetingSchedule.add(todaysDay, slot, BUSY);
		}
		return meetingSchedule;
	}
	
	@Test
	public void searchAvailableTimeSlotForDay() {
		LocalDate tomorrow = LocalDate.now().plusDays(1);
		attendeesSchedule.put(PERSON2, prepareBusySchedule(tomorrow, slotsArray));
		attendeesSchedule.put(PERSON1, busyScheduleForSpecificSlot(tomorrow, Meeting.EIGHT));
		MeetingAssistant assistant = new MeetingAssistant(attendeesSchedule, new Formatter(DATE));
		LocalDateTime availableTime = assistant.getCommonTimeSlot(meetingInvitee, tomorrow);
		assertEquals(LocalDateTime.of(tomorrow, Meeting.NINE), availableTime);
	}

	@Test
	public void searchTimeSlotWithFrank() {
		Map<String, MeetingScheduler> employeeSchedule = meetingScheduleForTeam();
		MeetingAssistant assistant = new MeetingAssistant(employeeSchedule, new Formatter(DATE));
		String availableTime = assistant.searchCommonSlotDetails(Arrays.asList(MINI, FRANK),
				LocalDate.now().plusDays(31));
		assertEquals(formatDate(LocalDate.now().plusDays(31), Meeting.TEN)
				+ ", is the first available time-slot for Mini and Frank to meet.", availableTime);
	}

	private Map<String, MeetingScheduler> meetingScheduleForTeam() {
		Map<String, MeetingScheduler> scheduler = new HashMap<>();
		LocalDate todaysDay = LocalDate.now().plusDays(30);
		LocalDate subSequentDay = LocalDate.now().plusDays(31);
		MeetingScheduler minisSchedule = minisSchedule(todaysDay, subSequentDay);
		MeetingScheduler bradsMeetingSchedule = prepareBradsSchedule(todaysDay, subSequentDay);
		MeetingScheduler janetsMeetingSchedule = janetsSchedule(todaysDay, subSequentDay);
		MeetingScheduler franksMeetingSchedule = prepareFranksSchedule(todaysDay, subSequentDay);
		scheduler.put(MINI, minisSchedule);
		scheduler.put(BRAD, bradsMeetingSchedule);
		scheduler.put(JANET, janetsMeetingSchedule);
		scheduler.put(FRANK, franksMeetingSchedule);
		return scheduler;
	}
	


	private String formatDate(LocalDate todayPlusThirtyOneDays, LocalTime time) {
		return DateTimeFormatter.ofPattern(DATE).format(LocalDateTime.of(todayPlusThirtyOneDays, time));
	}


	private MeetingScheduler prepareFranksSchedule(LocalDate todaysDay, LocalDate subSequentDay) {
		MeetingScheduler frank = new MeetingScheduler(todaysDay, subSequentDay);
		francksTodaysSchedule(todaysDay, frank);
		frank.add(subSequentDay, Meeting.EIGHT, MEET_BRAD);
		frank.add(subSequentDay, Meeting.NINE, MEET_JANET);
		frank.add(subSequentDay, Meeting.ELEVEN, LUNCH);
		frank.add(subSequentDay, Meeting.FIFTEEN, MEET_JANET);
		frank.add(subSequentDay, Meeting.SIXTEEN, BUSY);
		return frank;
	}

	private void francksTodaysSchedule(LocalDate todaysDay, MeetingScheduler frank) {
		frank.add(todaysDay, Meeting.TEN, TEAMPLANNING);
		frank.add(todaysDay, Meeting.ELEVEN, LUNCH);
		frank.add(todaysDay, Meeting.TWELVE, BUSY);
		frank.add(todaysDay, Meeting.THIRTEEN, MEET_BRAD);
		frank.add(todaysDay, Meeting.FOURTEEN, MEET_BRAD);
		frank.add(todaysDay, Meeting.FIFTEEN, BUSY);
		frank.add(todaysDay, Meeting.SIXTEEN, BUSY);
	}

	private MeetingScheduler janetsSchedule(LocalDate todaysDay, LocalDate subSequentDay) {
		MeetingScheduler jane = new MeetingScheduler(todaysDay, subSequentDay);
		jane.add(todaysDay, Meeting.NINE, MEET_MINI);
		jane.add(todaysDay, Meeting.TEN, TEAMPLANNING);
		jane.add(todaysDay, Meeting.TWELVE, LUNCH);
		jane.add(todaysDay, Meeting.THIRTEEN, BUSY);
		jane.add(todaysDay, Meeting.FOURTEEN, BUSY);
		janetssubSequentDayData(subSequentDay, jane);
		return jane;
	}

	private void janetssubSequentDayData(LocalDate subSequentDay, MeetingScheduler jane) {
		jane.add(subSequentDay, Meeting.EIGHT, MEET_MINI);
		jane.add(subSequentDay, Meeting.NINE, MEET_FRANK);
		jane.add(subSequentDay, Meeting.TEN, BUSY);
		jane.add(subSequentDay, Meeting.TWELVE, LUNCH);
		jane.add(subSequentDay, Meeting.THIRTEEN, BUSY);
		jane.add(subSequentDay, Meeting.FIFTEEN, MEET_FRANK);
	}

	private MeetingScheduler prepareBradsSchedule(LocalDate todaysDay, LocalDate subSequentDay) {
		MeetingScheduler brad = new MeetingScheduler(todaysDay, subSequentDay);
		bradstodaysDayData(todaysDay, brad);
		brad.add(subSequentDay, Meeting.EIGHT, MEET_FRANK);
		brad.add(subSequentDay, Meeting.NINE, MEET_MINI);
		brad.add(subSequentDay, Meeting.TWELVE, LUNCH);
		return brad;
	}

	private void bradstodaysDayData(LocalDate todaysDay, MeetingScheduler brad) {
		ministodaysDayData(todaysDay, brad, MEET_MINI, MEET_FRANK, TEAMPLANNING, LUNCH);
		brad.add(todaysDay, Meeting.SIXTEEN, BUSY);
	}

	private MeetingScheduler minisSchedule(LocalDate todaysDay, LocalDate subSequentDay) {
		MeetingScheduler mini = new MeetingScheduler(todaysDay, subSequentDay);
		ministodaysDayData(todaysDay, mini, MEET_BRAD, MEET_JANET, TEAMPLANNING, LUNCH);
		mini.add(subSequentDay, Meeting.EIGHT, MEET_JANET);
		mini.add(subSequentDay, Meeting.NINE, MEET_BRAD);
		mini.add(subSequentDay, Meeting.TWELVE, LUNCH);
		mini.add(subSequentDay, Meeting.FIFTEEN, BUSY);
		mini.add(subSequentDay, Meeting.SIXTEEN, BUSY);
		return mini;
	}

	private void ministodaysDayData(LocalDate todaysDay, MeetingScheduler minisSchedule, String meetBrad,
			String meetJanet, String teamPlanning, String lunch) {
		minisSchedule.add(todaysDay, Meeting.EIGHT, meetBrad);
		minisSchedule.add(todaysDay, Meeting.NINE, meetJanet);
		minisSchedule.add(todaysDay, Meeting.TEN, teamPlanning);
		minisSchedule.add(todaysDay, Meeting.TWELVE, lunch);
		minisSchedule.add(todaysDay, Meeting.FOURTEEN, BUSY);
		minisSchedule.add(todaysDay, Meeting.FIFTEEN, BUSY);
		minisSchedule.add(todaysDay, Meeting.SIXTEEN, BUSY);
	}

	private MeetingScheduler busyScheduleForSpecificSlot(LocalDate tomorrow, LocalTime time) {
		MeetingScheduler meetingSchedule = new MeetingScheduler(tomorrow);
		meetingSchedule.add(tomorrow, time, BUSY);
		return meetingSchedule;
	}

	private MeetingScheduler prepareBusySchedule(LocalDate endDate, LocalTime... values) {
		MeetingScheduler meetingSchedule = new MeetingScheduler(endDate);
		LocalDate nextDate = LocalDate.now();
		while (!nextDate.isAfter(endDate)) {
			for (LocalTime slot1 : values) {
				meetingSchedule.add(LocalDate.now(), slot1, BUSY);
			}
			nextDate = nextDate.plusDays(1);
		}
		return meetingSchedule;
	}
}