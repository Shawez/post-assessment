package com.xnsio.cleancode;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

public class MeetingScheduler {
	private static final String VALIDATION_OF_END_DATE = "End Date should be greater than Start Date";
	
	private final Map<LocalDate, Meeting> schedule = new LinkedHashMap<>();

	public MeetingScheduler(LocalDate endDate) {
		defaultScheduler(LocalDate.now(), endDate);
	}

	public MeetingScheduler(LocalDate startDate, LocalDate endDate) {
		if (startDate.isAfter(endDate)) {
			throw new RuntimeException(VALIDATION_OF_END_DATE);
		}
		defaultScheduler(startDate, endDate);
	}

	private void defaultScheduler(LocalDate startDate, LocalDate endDate) {
		LocalDate nextDate = startDate;
		while (!nextDate.isAfter(endDate)) {
			schedule.put(nextDate, new Meeting());
			nextDate = nextDate.plusDays(1);
		}
	}

	public void add(LocalDate day, LocalTime time, String description) {
		if (schedule.containsKey(day)) {
			Meeting meetings = schedule.get(day);
			meetings.add(time, description);
		}
	}

	public boolean isSlotAvailable(LocalDate day, LocalTime time) {
		return schedule.containsKey(day) && schedule.get(day).isTimeSlotAvailable(time);
	}


}
