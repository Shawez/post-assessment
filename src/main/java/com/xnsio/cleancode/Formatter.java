package com.xnsio.cleancode;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class Formatter {
	private static final String IS_THE_FIRST_AVAILABLE_TIME_SLOT_FOR = ", is the first available time-slot for ";
	private static final String TO_MEET = " to meet.";
	private static final String NO_AVAILABLE_TIME_SLOT_FOR = "No available time-slot for ";
	private final String format;

	public Formatter(String format) {
		this.format = format;
	}

	public String display(LocalDateTime availableTimeSlot, List<String> meetingInvitees) {
		String invitees = String.join(", ", meetingInvitees);
		int lastCommaIndex = invitees.lastIndexOf(",");
		invitees = invitees.substring(0, lastCommaIndex) + " and"
				+ invitees.substring(lastCommaIndex + 1);
		if (null == availableTimeSlot) {
			return NO_AVAILABLE_TIME_SLOT_FOR + invitees + TO_MEET;
		}
		String formattedDate = DateTimeFormatter.ofPattern(format).format(availableTimeSlot);
		return formattedDate + IS_THE_FIRST_AVAILABLE_TIME_SLOT_FOR + invitees + TO_MEET;
	}
}
