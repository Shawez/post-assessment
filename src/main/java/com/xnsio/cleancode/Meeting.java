package com.xnsio.cleancode;

import java.time.LocalTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Meeting {
	
	public static final LocalTime EIGHT = LocalTime.of(8, 0);
	public static final LocalTime NINE = LocalTime.of(9, 0);
	public static final LocalTime TEN = LocalTime.of(10, 0);
	public static final LocalTime ELEVEN = LocalTime.of(11, 0);
	public static final LocalTime TWELVE = LocalTime.of(12, 0);
	public static final LocalTime THIRTEEN = LocalTime.of(13, 0);
	public static final LocalTime FOURTEEN = LocalTime.of(14, 0);
	public static final LocalTime FIFTEEN = LocalTime.of(15, 0);
	public static final LocalTime SIXTEEN = LocalTime.of(16, 0);
	
	
	
	
	private static final String AVAILABLE = "AVAILABLE";
	private final Map<LocalTime, String> meetingSchedule = new HashMap<>();
	public static final List<LocalTime> slots = Arrays.asList(EIGHT, NINE, TEN, ELEVEN, TWELVE, THIRTEEN, FOURTEEN,	FIFTEEN, SIXTEEN);

	
	
	
	Meeting() {
		createDefaultSchedule();
	}

	private void createDefaultSchedule() {
		for (LocalTime time : slots) {
			meetingSchedule.put(time, AVAILABLE);
		}
	}

	void add(LocalTime time, String description) {
		meetingSchedule.put(time, description);
	}

	boolean isTimeSlotAvailable(LocalTime time) {
		return AVAILABLE.equals(meetingSchedule.get(time));
	}

}
