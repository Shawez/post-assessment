package com.xnsio.cleancode;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MeetingAssistant {
	private final Map<String, MeetingScheduler> teamScheduler;
	private final Formatter formatter;

	public MeetingAssistant(Map<String, MeetingScheduler> teamsScheduler, Formatter formatter) {
		this.teamScheduler = teamsScheduler;
		this.formatter = formatter;
	}

	public LocalDateTime getCommonTimeSlot(final List<String> attendees, final LocalDate endDate) {
		Collection<MeetingScheduler> meetingSchedules = scheduleAttendees(attendees).values();
		LocalDate nextDate = LocalDate.now();
		while (!nextDate.isAfter(endDate)) {
			final LocalDate currentDate = nextDate;
			for (LocalTime slot : Meeting.slots) {
				boolean avail = meetingSchedules.stream()
						.allMatch(meetingSchedule -> meetingSchedule.isSlotAvailable(currentDate, slot));
				if (avail) {
					return LocalDateTime.of(currentDate, slot);
				}
			}
			nextDate = nextDate.plusDays(1);
		}

		return null;
	}

	private Map<String, MeetingScheduler> scheduleAttendees(final List<String> meetingInvitee) {
		return teamScheduler.entrySet().stream().filter(entry -> meetingInvitee.contains(entry.getKey())).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}

	public String searchCommonSlotDetails(List<String> meetingInvitees, LocalDate endDate) {
		LocalDateTime commonSlot = getCommonTimeSlot(meetingInvitees, endDate);
		return formatter.display(commonSlot, meetingInvitees);
	}
}
